//
//  main.m
//  PointCloudScan
//
//  Created by Viet Trinh on 1/24/17.
//  Copyright © 2017 Viet Trinh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
