//
//  PCSViewController.m
//  PointCloudScan
//
//  Created by Viet Trinh on 1/24/17.
//  Copyright © 2017 Viet Trinh. All rights reserved.
//

#import "PCSViewController.h"
#define HAS_LIBCXX
#import <Structure/Structure.h>
#import <Structure/StructureSLAM.h>
#import <AVFoundation/AVFoundation.h>
#import <MobileCoreServices/MobileCoreServices.h>

struct CameraParameters{
    float cols;
    float rows;
    float fx;
    float fy;
    float cx;
    float cy;
};

@interface PCSViewController () <UIImagePickerControllerDelegate, AVCaptureVideoDataOutputSampleBufferDelegate, STSensorControllerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *colorImgView;
@property (weak, nonatomic) IBOutlet UIImageView *depthImgView;

@property (strong, nonatomic) AVCaptureSession* avCaptureSession;
@property (strong, nonatomic) AVCaptureDevice* avCaptureDevice;

@property (strong, nonatomic) STSensorController* STSensor;
@property (strong, nonatomic) NSString* STSensorStatus;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *pointCloudLabel;

@property (nonatomic) uint8_t *RGBDepthBuffer;
@property (nonatomic) struct CameraParameters depthCamParams;
@property (nonatomic) struct CameraParameters colorCamParams;
@end

@implementation PCSViewController


#pragma mark - View Controller Lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.STSensor = [STSensorController sharedController];
    self.STSensor.delegate = self;
    self.RGBDepthBuffer = NULL;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if(![self deviceHasCamera])
        [self popupMessage:@"The device does not have a camera !" withTitle:@"Point Cloud Scan"];
    else{
        [self setupCamerasParameters];
        [self setupColorCamera];
        [self startColorCamera];
        [self setupSTSensor];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(appBecomeActive)
                                                     name:UIApplicationDidBecomeActiveNotification
                                                   object:nil];
    }
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self stopColorCamera];
    self.colorImgView = nil;
    self.depthImgView = nil;
    self.avCaptureSession = nil;
    self.avCaptureDevice = nil;
    self.STSensorStatus = nil;
    free(self.RGBDepthBuffer);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)updateStatusLabel:(NSString*)status{
    if ([status isEqual: SENSOR_STATUS_OK])
        self.statusLabel.hidden = YES;
    else{
        self.statusLabel.text = status;
        self.statusLabel.hidden = NO;
    }
}

- (void)appBecomeActive{
    [self setupSTSensor];
}


#pragma mark - UIImagePickerControllerDelegate
-(BOOL)deviceHasCamera{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        NSArray* availableMediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
        if ([availableMediaTypes containsObject:(NSString*)kUTTypeImage]) {
            return YES;
        }
    }
    return NO;
}


#pragma mark - UIAlertController
-(void)popupMessage:(NSString*)message withTitle:(NSString*)title{
    UIAlertController* alertController = [UIAlertController alertControllerWithTitle:title
                                                                             message:message
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* alertAction = [UIAlertAction actionWithTitle:@"OK"
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * _action) {
                                                            [alertController dismissViewControllerAnimated:YES
                                                                                                completion:nil];
                                                        }];
    [alertController addAction:alertAction];
    [self presentViewController:alertController animated:YES completion:nil];
}


#pragma mark - AVFoundation
- (void)setupCamerasParameters{
    _depthCamParams.cols = VGA_COLS;
    _depthCamParams.rows = VGA_ROWS;
    _depthCamParams.fx   = VGA_FX/VGA_COLS*_depthCamParams.cols;
    _depthCamParams.fy   = VGA_FY/VGA_ROWS*_depthCamParams.rows;
    _depthCamParams.cx   = VGA_CX/VGA_COLS*_depthCamParams.cols;
    _depthCamParams.cy   = VGA_CY/VGA_ROWS*_depthCamParams.rows;
    
    _colorCamParams.cols = VGA_COLS;
    _colorCamParams.rows = VGA_ROWS;
    _colorCamParams.fx   = iPAD_FX/VGA_COLS*_colorCamParams.cols;
    _colorCamParams.fy   = iPAD_FY/VGA_ROWS*_colorCamParams.rows;
    _colorCamParams.cx   = iPAD_CX/VGA_COLS*_colorCamParams.cols;
    _colorCamParams.cy   = iPAD_CY/VGA_ROWS*_colorCamParams.rows;
}

- (bool)authorizeToUseColorCamera{
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if (authStatus != AVAuthorizationStatusAuthorized) {
        NSLog(@"Authorizing color-camera usage for the app");
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            if (granted) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self startColorCamera];
                });
            }
        }];
        return false;
    }
    return true;
}

- (void)setupColorCamera{
    if (_avCaptureSession) {
        NSLog(@"AVCaptureSession is running. Cannot set a new configuration !");
        return;
    }
    
    bool isColorCameraAuthorizedToUse = [self authorizeToUseColorCamera];
    if (!isColorCameraAuthorizedToUse){
        NSLog(@"Camera is not authorized to use. App exits !");
        return;
    }
    
    /* 1. Setup AVCaptureSession */
    self.avCaptureSession = [[AVCaptureSession alloc] init];
    [self.avCaptureSession beginConfiguration];
    self.avCaptureSession.sessionPreset = AVCaptureSessionPreset640x480;
    
    /* 2. Setup AVCaptureDevice with Configuration */
    self.avCaptureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    NSError* avCaptureDeviceConfigError;
    if ([self.avCaptureDevice lockForConfiguration:&avCaptureDeviceConfigError]) {
        // Allow exposure to change
        if ([self.avCaptureDevice isExposureModeSupported:AVCaptureExposureModeContinuousAutoExposure])
            [self.avCaptureDevice setExposureMode:AVCaptureExposureModeContinuousAutoExposure];
        
        // Allow white balance to change
        if ([self.avCaptureDevice isWhiteBalanceModeSupported:AVCaptureWhiteBalanceModeContinuousAutoWhiteBalance])
            [self.avCaptureDevice setWhiteBalanceMode:AVCaptureWhiteBalanceModeContinuousAutoWhiteBalance];
        
        // Set focus at the maximum position allowable to get the best color/depth alignment.
        [self.avCaptureDevice setFocusModeLockedWithLensPosition:1.0f completionHandler:nil];
        
        [self.avCaptureDevice unlockForConfiguration];
    }
    
    /* 3. Add an avCaptureDevice input into AVCaptureSession */
    AVCaptureDeviceInput* avInput = [AVCaptureDeviceInput deviceInputWithDevice:self.avCaptureDevice
                                                                          error:&avCaptureDeviceConfigError];
    if (!avInput || avCaptureDeviceConfigError){
        NSLog(@"Cannot initialize AVCaptureDeviceInput. App exits !");
        return;
    }
    [self.avCaptureSession addInput:avInput];
    
    /* 4. Setup AVCaptureDataOutput for camera display and then add an avCaptureOutput into AVCaptureSession */
    AVCaptureVideoDataOutput* avOutput = [[AVCaptureVideoDataOutput alloc] init];
    [avOutput setAlwaysDiscardsLateVideoFrames:YES];
    avOutput.videoSettings = @{(NSString*)kCVPixelBufferPixelFormatTypeKey : @(kCVPixelFormatType_32BGRA)};
    [avOutput setSampleBufferDelegate:self queue:dispatch_get_main_queue()];
    [self.avCaptureSession addOutput:avOutput];
    
    if([self.avCaptureDevice lockForConfiguration:&avCaptureDeviceConfigError]) {
        [self.avCaptureDevice setActiveVideoMaxFrameDuration:CMTimeMake(1, 30)];
        [self.avCaptureDevice setActiveVideoMinFrameDuration:CMTimeMake(1, 30)];
        [self.avCaptureDevice unlockForConfiguration];
    }
    
    [self.avCaptureSession commitConfiguration];
}

- (void)startColorCamera{
    if (_avCaptureSession && [self.avCaptureSession isRunning])
        return;
    
    if (!_avCaptureSession)
        [self setupColorCamera];
    
    [self.avCaptureSession startRunning];
}

- (void)stopColorCamera{
    if([self.avCaptureSession isRunning])
        [self.avCaptureSession stopRunning];
    self.avCaptureSession = nil;
    self.avCaptureDevice = nil;
}

/*
    AVCaptureVideoDataOutputSampleBufferDelegate, where
    (CMSampleBufferRef)sampleBuffer holds RGBA values for each camera frame
 */
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection{
    [self.STSensor frameSyncNewColorBuffer:sampleBuffer];
}


#pragma mark - STSensorControllerDelegate
- (void)setupSTSensor{
    STSensorControllerInitStatus initStatus = [self.STSensor initializeSensorConnection];
    if (initStatus == STSensorControllerInitStatusSuccess || initStatus == STSensorControllerInitStatusAlreadyInitialized) {
        self.STSensorStatus = SENSOR_STATUS_OK;
        [self updateStatusLabel:self.STSensorStatus];
        
        [self startColorCamera];
        
        // Request to receive depth frames with synchronized color pairs, through delegate methods
        STStreamConfig streamConfig = STStreamConfigDepth640x480;
        NSError* streamError;
        BOOL isStreamSuccessful = [self.STSensor startStreamingWithOptions:@{kSTStreamConfigKey : @(streamConfig),
                                                                             kSTFrameSyncConfigKey: @(STFrameSyncDepthAndRgb),
                                                                             kSTHoleFilterConfigKey: @TRUE}
                                                                     error:&streamError];
        if (!isStreamSuccessful)
            NSLog(@"Streaming Error: %s",[[streamError localizedDescription] UTF8String]);
    }
    else{
        if (initStatus == STSensorControllerInitStatusSensorNotFound)
            NSLog(@"No Structure Sensor found!");
        else if (initStatus == STSensorControllerInitStatusOpenFailed)
            NSLog(@"Structure Sensor open failed.");
        else if (initStatus == STSensorControllerInitStatusSensorIsWakingUp)
            NSLog(@"Structure Sensor is waking from low power.");
        else if (initStatus != STSensorControllerInitStatusSuccess)
            NSLog(@"Structure Sensor failed to init with status %d.", (int)initStatus);
        
        self.STSensorStatus = SENSOR_STATUS_NEED_TO_CONNECT;
        [self updateStatusLabel:self.STSensorStatus];
    }
}

- (void)sensorDidConnect{
    NSLog(@"Structure Sensor is connected");
    [self setupSTSensor];
}

- (void)sensorDidDisconnect{
    NSLog(@"Structure Sensor is disconnected");
    self.STSensorStatus = SENSOR_STATUS_NEED_TO_CONNECT;
    [self updateStatusLabel:self.STSensorStatus];
    [self stopColorCamera];
}

- (void)sensorDidLeaveLowPowerMode{
    self.STSensorStatus = SENSOR_STATUS_NEED_TO_CONNECT;
    [self updateStatusLabel:self.STSensorStatus];
}

- (void)sensorBatteryNeedsCharging{
    self.STSensorStatus = SENSOR_STATUS_NEED_TO_CHARGE;
    [self updateStatusLabel:self.STSensorStatus];
}

- (void)sensorDidStopStreaming:(STSensorControllerDidStopStreamingReason)reason{
    [self stopColorCamera];
}

- (void)sensorDidOutputSynchronizedDepthFrame:(STDepthFrame *)depthFrame andColorFrame:(STColorFrame *)colorFrame{
    [self renderDepthFrame:depthFrame andColorFrame:colorFrame];
}


#pragma mark - Rendering
- (void)renderDepthFrame:(STDepthFrame*)depthFrame andColorFrame:(STColorFrame*)colorFrame{
    /* Process STDepthFrame*/
    // 1. Display depthFrame image
    self.depthImgView.image = [self UImageFromSTDepthFrame:depthFrame];
    
    // 2. Pixel coordinates of center of a screen
    int Xs = depthFrame.width/2;
    int Ys = depthFrame.height/2;
    
    // 3. Convert depth-camera coordinates into world coordinates
    const float* depths = [depthFrame depthInMillimeters];
    float depth = depths[Ys*depthFrame.width + Xs]*0.001;
    float Xw = depth*(Xs - self.depthCamParams.cx)/self.depthCamParams.fx;
    float Yw = depth*(Ys - self.depthCamParams.cy)/self.depthCamParams.fy;
    float Zw = depth;
    
    /* Process STColorFrame */
    // 1. Display colorFrame image
    self.colorImgView.image = [self UImageFromCMSampleBufferRef:colorFrame.sampleBuffer];
    
    // 2. Get RGBA components at the center pixel (Xs, Ys)
    CFDataRef pixelData = CGDataProviderCopyData(CGImageGetDataProvider(self.colorImgView.image.CGImage));
    const UInt8* rgbaData = CFDataGetBytePtr(pixelData);
    int offset = ((self.colorImgView.image.size.width * Ys) + Xs) * 4;
    float red   = rgbaData[offset + 0]/255.0f;
    float green = rgbaData[offset + 1]/255.0f;
    float blue  = rgbaData[offset + 2]/255.0f;
    float alpha = rgbaData[offset + 3]/255.0f;
    
    // 3. Update labels
    [self.pointCloudLabel setText:[NSString stringWithFormat:@"RGBA = (%.2f, %.2f, %.2f, %.2f); (X, Y, Z) = (%.2f cm, %.2f cm, %.2f cm)",red, green, blue, alpha, Xw*100,Yw*100,Zw*100]];
    
    /* Release allocated memories of UIImage */
    CFRelease(pixelData);
    [self releaseImageData:self.depthImgView.image];
    [self releaseImageData:self.colorImgView.image];
}

- (UIImage*)UImageFromSTDepthFrame:(STDepthFrame*)depthFrame{
    NSError* error = nil;
    STDepthToRgba* depth2RBGA = [[STDepthToRgba alloc] initWithOptions:@{kSTDepthToRgbaStrategyKey:@(STDepthToRgbaStrategyRedToBlueGradient)}
                                                                 error:&error];
    self.RGBDepthBuffer = [depth2RBGA convertDepthFrameToRgba:depthFrame];
    
    size_t cols = depthFrame.width;
    size_t rows = depthFrame.height;
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    NSData *data = [NSData dataWithBytes:self.RGBDepthBuffer length:cols*rows*4];
    CGBitmapInfo bitmapInfo;
    bitmapInfo = (CGBitmapInfo)kCGImageAlphaNoneSkipLast;
    bitmapInfo |= kCGBitmapByteOrder32Big;
    
    CGDataProviderRef provider = CGDataProviderCreateWithCFData((CFDataRef)data);
    CGImageRef imageRef = CGImageCreate(cols,rows,8,8*4,cols*4,colorSpace,bitmapInfo,provider,NULL,false,kCGRenderingIntentDefault);
    
    return [UIImage imageWithCGImage:imageRef];
}

- (UIImage*)UImageFromCMSampleBufferRef:(CMSampleBufferRef)sampleBuffer{
    CVImageBufferRef pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    CVPixelBufferLockBaseAddress(pixelBuffer, 0);
    
    size_t cols = CVPixelBufferGetWidth(pixelBuffer);
    size_t rows = CVPixelBufferGetHeight(pixelBuffer);
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    unsigned char *ptrPixelBuffer = (unsigned char *) CVPixelBufferGetBaseAddressOfPlane(pixelBuffer, 0);
    
    NSData *data = [[NSData alloc] initWithBytes:ptrPixelBuffer length:rows*cols*4];
    CVPixelBufferUnlockBaseAddress(pixelBuffer, 0);
    CGBitmapInfo bitmapInfo;
    bitmapInfo = (CGBitmapInfo)kCGImageAlphaNoneSkipFirst;
    bitmapInfo |= kCGBitmapByteOrder32Little;
    
    CGDataProviderRef provider = CGDataProviderCreateWithCFData((CFDataRef)data);
    CGImageRef imageRef = CGImageCreate(cols, rows, 8, 8*4, cols*4, colorSpace, bitmapInfo, provider, NULL, false, kCGRenderingIntentDefault);
    
    return [[UIImage alloc] initWithCGImage:imageRef];
}

- (void)releaseImageData:(UIImage*)image{
    if (image) {
        CGImageRelease(image.CGImage);
        CGDataProviderRelease(CGImageGetDataProvider(image.CGImage));
        CGColorSpaceRelease(CGImageGetColorSpace(image.CGImage));
    }
}

@end
