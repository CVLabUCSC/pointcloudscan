//
//  AppDelegate.h
//  PointCloudScan
//
//  Created by Viet Trinh on 1/24/17.
//  Copyright © 2017 Viet Trinh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

