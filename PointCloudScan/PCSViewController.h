//
//  PCSViewController.h
//  PointCloudScan
//
//  Created by Viet Trinh on 1/24/17.
//  Copyright © 2017 Viet Trinh. All rights reserved.
//

#import <UIKit/UIKit.h>

/*
 Intrinsic Parameters for Depth and Color Camera
 
 1. Depth Intrinsics for the Structure Sensor:
 Focal Length in Pixels:
 f_QVGA = (285.25, 285.25)
 f_VGA  = (570.50, 570.50)
 Principal Point:
 c_QVGA = (160, 120)
 c_VGA  = (320, 240)
 Distortion:
 K1 = -0.024
 K2 = 0
 
 2. Color Intrinsics assuming VGA resolution:
 iPad Air 1
 Focal length in pixels = (582, 582)
 Center = (320, 240)
 K1, K2 Distortion = 0.124, -0.214
 
 iPad Air 2
 Focal length in pixels = (578, 578)
 Center = (320, 240)
 K1, K2 Distortion = 0.126, -0.236
 
 3. Resolution:
 QVGA: width, height = 320, 240
 VGA:  width, height = 640, 480
 */

/* Using VGA Resolution in this application */
#define VGA_COLS 640
#define VGA_ROWS 480
#define VGA_FX   570.5
#define VGA_FY   570.5
#define VGA_CX   320
#define VGA_CY   240
#define iPAD_FX  578
#define iPAD_FY  578
#define iPAD_CX  320
#define iPAD_CY  240

#define SENSOR_STATUS_OK @"Structore Sensor Status Ok"
#define SENSOR_STATUS_NEED_TO_CONNECT @"Please connect your Structure Sensor"
#define SENSOR_STATUS_NEED_TO_CHARGE @"Please charge your Structure Sensor"

@interface PCSViewController : UIViewController

@end

